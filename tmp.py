# %% 
import pandas as pd
import numpy as np
import seaborn as sb
# %% Define a function for preparing our training and testing data...
def prep_data(phenotype) :
    pheno = pd.read_csv('./input/gono-unitigs/metadata.csv', index_col=0)
    pheno = pheno.dropna(subset=[phenotype]) # drop samples that don't have a value for our chosen resistance profile
    pheno = pheno[phenotype]
        
    # read in unitig data
    X = pd.read_csv('./input/gono-unitigs/' + phenotype + '_gwas_filtered_unitigs.Rtab', sep=" ", index_col=0, low_memory=False)
    X = X.transpose()
    X = X[X.index.isin(pheno.index)] # only keep rows with a resistance measure
    pheno = pheno[pheno.index.isin(X.index)]
    return X, pheno

# %% Prepare the data...
# prepare our data for predicting antibiotic resistance
# antibiotic = "Azithromycin"
# phenotype = 'azm_sr'
# antibiotic = "Ciprofloxacin"
# phenotype = 'cip_sr'
antibiotic = "Ceftriaxone"
phenotype = 'cfx_sr'

X, pheno = prep_data(phenotype)

# create an array for storing performance metrics
performance = {
    "balanced_accuracy": [],
    "accuracy": [],
    "average_precision": [],
    "f1_score": [],
    "precision": [],
    "recall": []
}
method = []
times = []

# %% Look at the length distribution of the unitigs in our dataset:
unitigs = X.columns
mylen = np.vectorize(len)
uni_len = mylen(unitigs)
sb.distplot(uni_len)
# %%
print(X.shape)
# %%
