# Machine Learning to Predict AMR

This repository includes a base dataset and code to test machine learning algorithms for predicting antimicrobial resistance from DNA sequence reads.

## Running the Code

1. Install Anaconda.
2. Create a new environment with python 3.7.
3. Install the required pythom modules in a new conda environment.
4. Activate the new python environment and run `interactive-python.py`.

VSCode is recommended. It allows each section of the `interactive-python.py` to be run separately.